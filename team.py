from jogador import Jogador


class Time:

    def __init__(self, n, x):
        self.x = x
        self.jogadores = []
        self.jogadores.append(Jogador(False))
        for i in range(1, n):
            self.jogadores.append(Jogador(True))

    def update_status(self):
        flag = True
        for jogador in self.jogadores:
            jogador.update_status()
            if flag and jogador.is_free():
                jogador.run(self.x)
                flag = False

    def free_players(self) -> int:
        result = 0
        for jogador in self.jogadores:
            result += jogador.is_free()
        return result

    def print_players(self):
        result = []
        for jogador in self.jogadores:
            result.append(jogador.is_free())
        print(result)


