from team import Time
from jogador import Jogador


t = int(input("Quantidade de segundos restantes até o fim da jogada: "))
if t <= 0 or t > 24:
    print("Valot de T inválido")

q = int(input("Quantidade de jogadores em quadra: "))
if q < 2 or q > 5:
    print("Valor de Q inválido")

x = int(input("Tempo de reposicionamento dos jogadores: "))
if x <= 0 or x >= q:
    print("Valor de X inválido") 


sum = 0
time = Time(q, x)

for i in range(t):
    time.update_status()
    sum += time.free_players()

print(sum)

