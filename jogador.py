class Jogador:

    def __init__(self, is_free):
        if is_free:
            self.cont = 0
        else:
            self.cont = 1

    def run(self, n):
        self.cont += n

    def update_status(self):
        if not self.is_free():
            self.cont -= 1

    def is_free(self) -> bool:
        return self.cont == 0

